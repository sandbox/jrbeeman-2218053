Download the CKEditor BBCode addon and extract it to /libraries/ckeditor_bbcode
or use Drush Make along with the sample contents in ckeditor_bbcode.make.example 
to download the library.